//////////////
//// CSE 02 TicTacToe
/// Collin Hanlon
//////////////

import java.util.Arrays;
import java.util.Scanner;
  public class TicTacToe{//Start Class
    public static void main(String[] args){//Start main
      Scanner myScanner = new Scanner( System.in ); 
      char[][] ticTacArray = { //Original tic tac toe board
        {'1', '2', '3'},
        {'4', '5', '6'},
        {'7', '8', '9'},
      };
      //Prints out the aray
      for (int row = 0; row < ticTacArray.length; row++){
        for (int column = 0; column < ticTacArray[row].length; column++){
          System.out.print (ticTacArray[row][column] + " ");
        }
        System.out.println();
      }
      //counter
      int j = 0;
     while (j < 10){
       if (j < 9){
         System.out.println("Please enter either 'x' or 'o'."); //Enter player
        char player = myScanner.next().charAt(0); //save the player
        if (player == 'x' || player == 'X'){
          System.out.println("Enter the number of the spot you want to replace."); //pick which spot 1-9
          int position = myScanner.nextInt();
          if (position > 0 && position <= 9){
            int value = check(ticTacArray, player, position); //Check to see if value is already being used
            if (value == 0){
                TicTac(ticTacArray, player, position); //Prints the updated game board
                boolean win = winner(ticTacArray); //checks to see if a winner has been found
                if (win == true){ //When a player wins
                  System.out.println("You won!"); //Prints win statment
                  j = 10;
                }else { // when there is no winner after a turn
                  j++;}
              }else if (value == 1){ //Value is taken
                System.out.println("Error, please enter a number not used."); //new number
              }}
          else{ //if # isnt 1-9
            System.out.println ("Error, please enter an int from 1 to 9!");}
        }else if (player == 'o' || player == 'O'){//check to see if they are player o, if they are continue 
          System.out.println("Enter the number of the spot you want to replace.");//pick which spot 1-9
          int position = myScanner.nextInt();
          if (position > 0 && position <= 9){ // If the position is in the domain 1-9
            int value = check(ticTacArray, player, position); //Checks if value is being used
              if (value == 0){
                TicTac(ticTacArray, player, position);  //Prints the updated game board
                boolean win = winner(ticTacArray); //checks to see if a winner has been found
                if (win == true){ //When a player wins
                  System.out.println("You won!"); //Print win statment
                  j = 10;}
                else {// When there is no winner after the turn
                  j++;}}
              else if (value == 1){ //if value = 1 then that position is already taken
                System.out.println("Error, please enter a number not used.");}}
          else{  //When the input is outside 1-9
            System.out.println ("Error, please enter an int from 1 to 9!");}}//Ask user for different #
        else if (player != 'x' && player != 'o'){ //If neither an x or o is entered
          System.out.println("Invalid input. Please select 'x' or 'o'");}}
       else if (j == 9){ //for when there is a draw
         System.out.println("Draw!"); //Prints a draw 
         j++;}}}//End of Main method
    public static void TicTac (char [] [] beforeArray, char player, int position){
      //Switch statement which finds the # being swapped for
      switch (position){
          case 1: beforeArray [0][0] = player;
            break;
          case 2: beforeArray [0][1] = player;
            break; 
          case 3: beforeArray [0][2] = player;
            break; 
          case 4: beforeArray [1][0] = player;
            break; 
          case 5: beforeArray [1][1] = player;
            break; 
          case 6: beforeArray [1][2] = player;
            break;
          case 7: beforeArray [2][0] = player;
            break;
          case 8: beforeArray [2][1] = player;
            break;
          case 9: beforeArray [2][2] = player;
            break;
          default: System.out.println ("Invalid. Must be a number from 1-9.");
            break;}
      for (int row = 0; row < beforeArray.length; row++){
        for (int column = 0; column < beforeArray[row].length; column++){
            System.out.print (beforeArray[row][column] + " ");} //prints new array
        System.out.println();}}//End ofmethod 
    public static int check (char [] [] beforeArray, char player, int position){ //Checks whether there is an x or o already 
      int checkValue = 0; 
      //Switch statement that takes the position requested and sees if it is taken, if taken, reprompts you for spot
      switch (position){
          case 1: if (beforeArray [0][0] == 'x' || beforeArray [0][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 2: if (beforeArray [0][1] == 'x' || beforeArray [0][1] == 'o'){
                checkValue = 1;}else{
                checkValue = 0;}
                break; 
          case 3: if (beforeArray [0][2] == 'x' || beforeArray [0][2] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break; 
          case 4: if (beforeArray [1][0] == 'x' || beforeArray [1][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break; 
          case 5: if (beforeArray [1][1] == 'x' || beforeArray [1][1] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break; 
          case 6: if (beforeArray [1][2] == 'x' || beforeArray [1][2] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 7: if (beforeArray [2][0] == 'x' || beforeArray [2][0] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 8: if (beforeArray [2][1] == 'x' || beforeArray [2][1] == 'o'){
                checkValue = 1;} else{
                checkValue = 0;}
            break;
          case 9: if (beforeArray [2][2] == 'x' || beforeArray [2][2] == 'o'){
                checkValue = 1;} else {
                checkValue = 0;}
            break;
          default: System.out.println ("Invalid.");
            break;}
       return checkValue;}//End check method 
    public static boolean winner (char [][] finalArray){ //Checks to see if a winner is found in the current array 
      //If statements go through every possibility 
      if (finalArray [0][0] == 'x' && finalArray [0][1] == 'x' && finalArray [0][2] == 'x'){
        return true;}
      else if (finalArray [1][0] == 'x' && finalArray [1][1] == 'x' && finalArray [1][2] == 'x'){
        return true;}
      else if (finalArray [2][0] == 'x' && finalArray [2][1] == 'x' && finalArray [2][2] == 'x'){
        return true;}
      else if (finalArray [0][0] == 'x' && finalArray [0][1] == 'x' && finalArray [0][2] == 'x'){
        return true;}
      else if (finalArray [1][0] == 'x' && finalArray [1][1] == 'x' && finalArray [1][2] == 'x'){
        return true;}
      else if (finalArray [2][0] == 'x' && finalArray [2][1] == 'x' && finalArray [2][2] == 'x'){
        return true;}
      else if (finalArray [0][0] == 'x' && finalArray [1][1] == 'x' && finalArray [2][2] == 'x'){
        return true;}
      else if (finalArray [0][2] == 'x' && finalArray [1][1] == 'x' && finalArray [2][0] == 'x'){
        return true;}
      else if (finalArray [0][0] == 'o' && finalArray [0][1] == 'o' && finalArray [0][2] == 'o'){
        return true;}
      else if (finalArray [1][0] == 'o' && finalArray [1][1] == 'o' && finalArray [1][2] == 'o'){
        return true;}
      else if (finalArray [2][0] == 'o' && finalArray [2][1] == 'o' && finalArray [2][2] == 'o'){
        return true;}
      else if (finalArray [0][0] == 'o' && finalArray [0][1] == 'o' && finalArray [0][2] == 'o'){
        return true;}
      else if (finalArray [1][0] == 'o' && finalArray [1][1] == 'o' && finalArray [1][2] == 'o'){
        return true;}
      else if (finalArray [2][0] == 'o' && finalArray [2][1] == 'x' && finalArray [2][2] == 'o'){
        return true;}
      else if (finalArray [0][0] == 'o' && finalArray [1][1] == 'o' && finalArray [2][2] == 'o'){
        return true;}
      else if (finalArray [0][2] == 'o' && finalArray [1][1] == 'o' && finalArray [2][0] == 'o'){
        return true;}
      else{
        return false;}
    }//end of method 
  }//end of class