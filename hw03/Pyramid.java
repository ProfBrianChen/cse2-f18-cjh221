//////////////
//// CSE 02 Pyramid
/// Collin Hanlon
import java.util.Scanner;
//
//
//
public class Pyramid{
    //accepting input for the height and length of the square base of a pyramid and calculating the volume within it
  public static void main(String[] args) {
   Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the is (input length) : "); //inputted value of the side of the square base
    double baseLength = myScanner.nextDouble(); //defining the length of the square base
    System.out.print("The height of the pyramid is (input height) : "); //inputted value of the height of the pyramid
    double pyramidHeight = myScanner.nextDouble(); //defining the height of the pyramid
    double pyramidVolume = (int)((baseLength * baseLength) * (pyramidHeight / 3)); //calculating the volume enclosed within the pyramid of said dimensions
    System.out.println("The volume inside the pyramid is " + pyramidVolume + '.'); //outputting the volume
  
  } //end of main method
} //end of class