//////////////
//// CSE 02 Convert
/// Collin Hanlon
import java.util.Scanner;
//
//
//
public class Convert{
  //accepting input for the acres affected and the amount of rainfall and converting that into cubic miles of rainfall
  public static void main(String[] args) {
   Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter the affected area in acres: "); //inputted value of acres
    double rainArea = myScanner.nextDouble(); //defining the acres affected
    System.out.print("Enter rainfall in the affected area in inches: "); //inputted value of rainfall
    double rainFall = myScanner.nextDouble(); //defining the rainfall affected
    double cubicRainFall = (rainArea * .0015625) * (rainFall / 63360); //calculating the rainfall in cubic miles
    System.out.println("The rain that fell amounted to " + cubicRainFall + " cubic miles."); //outputting the rainfall
  } //end of main method
  
 
} //end of class