//////////////
//// CSE 02 Linear
/// Collin Hanlon
//////////////

import java.util.Scanner;// imports Scanner
import java.util.Arrays;// imports Arrays
public class CSE2Linear//class
{
    public static void main (String[]args){// main method
        System.out.println("Please Enter 15 ints: ");// prompts user
        Scanner input = new Scanner(System.in);// Scanner
        int counter = 0;
        int[] array1 = new int[15];
        while (counter < 15){
            if (input.hasNextInt()){// checks if input is an integer
                array1[counter] = input.nextInt();// saves as int
                if (array1[counter] < 0 || array1[counter] > 100){// ensures valid input
                    System.out.println("Integer is not within the range 0-100");// prints error
                    System.exit(0);}
                else{
                    if (counter > 0){
                        if (array1[counter] < array1[counter - 1]){// ensures input is greater than previous
                            System.out.println("The entered intger is not greater than the previous one ");//prints error if not
                            System.exit(0);}}
                counter++;}}
            else{
                System.out.println("An Integer was not entered");// prints error message
                System.exit(0);}}
        System.out.println("Sorted Integers: ");
        System.out.println(Arrays.toString(array1));// converts array to be printed out
        System.out.print("Enter a grade to search: ");// search
        if (input.hasNextInt()){// makes sure int is entered
            int x = input.nextInt();// if int is inputted, saved as int
            BinarySearch(array1, x);
            Scramble(array1);
            System.out.println("Scrambled:");
            System.out.println(Arrays.toString(array1));// coverts scrambled array to be printed out
            System.out.print("Enter a grade to search: ");// prompts user to search for
            if (input.hasNextInt()){// ensures input is an int
                LinearSearch(array1,x);}
            else{
                System.out.println("An Integer was not entered");
                System.exit(0);}}
        else{
            System.out.println("A grade was not entered to seach for");
            System.exit(0);}}
    public static void LinearSearch(int[] array1, int x){// LinearSearch method
        for (int i = 0; i < array1.length; i++){
            if (array1[i] == x){// checks if number is equal to the user input
                System.out.println(x + " was found in " + (i + 1) + " iterations");// prints where int was found
                break;}
            else if (array1[i] != x && i == (array1.length - 1)){//if int isn't found after 15 iterations
                System.out.println(x + " was not found in " + (i + 1) + " iterations");}}}
    public static int[] Scramble(int[] array1){// Scramble
        for (int i = 0; i < array1.length; i++){
            int x = (int)(Math.random()*array1.length);// random number 
            int store = array1[i];
            while (x != i){
                array1[i] = array1[x];
                array1[x] = store;
                break;}}
        return array1;}
    public static void BinarySearch(int[] array2, int integer){
        int highIndex = array2.length - 1;
        int lowIndex = 0;
        int counter = 0;
        while (lowIndex <= highIndex){
            counter++;
            int midIndex = (highIndex + lowIndex)/2;
            if (integer < array2[midIndex]){
                highIndex = midIndex - 1;}
            else if (integer > array2[midIndex]){
                lowIndex = midIndex + 1;}
            else if (integer == array2[midIndex]){
                System.out.println(integer + " was found in " + counter + " iterations");
                break;}}
        if (lowIndex > highIndex){
            System.out.println(integer + " was not found in " + counter + " iterations");}}}