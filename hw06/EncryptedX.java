//////////////
//// CSE 002 HW06 EncryptedX
/// Collin Hanlon
//////////////

import java.util.Scanner; //Importing Scanner

public class EncryptedX {
  public static void main(String[] args) {
    Scanner myScanner=new Scanner (System.in); //creating a scanner 
    System.out.print("Enter an integer between 1-100: "); //prompting the user for # of lines
    int enteredVal=myScanner.nextInt(); //collected inputted value
    
    if(enteredVal > 100||enteredVal <1){ //Ensuring entered value is within range of 1-100
      System.out.print("Invalid input, reenter a value between 1-100: "); //reprompting user for correct value
      enteredVal=myScanner.nextInt(); //recollecting value
    }
    else{
      for(int row=0; row<enteredVal; row++){ //rows
        System.out.println(""); //printing
        for(int column =0; column<enteredVal; column++){ //columns
          if(row==column||column==(enteredVal-row-1)){ //adding in the spaces which create the X pattern 
            System.out.print(" "); //printing the gaps for X
          }
        else{
          System.out.print("*"); //printing the stars to fill in
        }}}}
  }//end of main method
}//end of class

  