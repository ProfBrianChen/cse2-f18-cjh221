//////////////
//// CSE 02 SHuffling
/// Collin Hanlon
//////////////
import java.util.Scanner; //Scanner
import java.util.Arrays; //Array
import java.util.Random; //Random

public class Shuffling{
public static void main(String[] args){

Scanner scan = new Scanner(System.in);
String[] suitNames={"C","H","S","D"};
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
String[] cards = new String[52]; 
String[] hand = new String[5]; 
    int intTest = 1; //testing variable 
    int numCards = 0;
      
      while(intTest < 2){
      System.out.println("How many cards do you want in your hand (0-52)? "); //asking for input
      boolean correctInt = scan.hasNextInt(); //integer check
        if (correctInt){
          numCards = scan.nextInt(); 
          if (numCards > 0 && numCards < 53){  //valid input
            intTest++;}}
        else {
            scan.next();
            System.out.println("Invalid input, please reenter an integer between 0-52");}} //invalid input

int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" ");} 

  System.out.println();
printArray(cards, numCards); 
shuffle(cards); 
printArray(cards, numCards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand, numCards);
   index = index - numCards;
   System.out.println("Enter 1 to draw another hand"); //redrawing
   again = scan.nextInt();}  
  }//main 

public static void printArray(String[] list, int numCards){
  if(list.length == 52){
  for (int i=0; i<52; i++){
  System.out.print(list[i] +" ");}
  System.out.println();}
  else{
    for (int i=0; i<numCards; i++){
    System.out.print(list[i] +" ");}
   System.out.println();}
}//main

public static String[] shuffle(String[] list){
   Random randGen = new Random(); //number generator
   System.out.println("Shuffled");
   //int count = 0;
        for(int i=0; i<52; i++){
          int r = randGen.nextInt(50)+1;//generates a number 0-51
          String temp = list[0];
          list[0] = list[r];
          list[r] = temp;
         // count++;
        }     
       // System.out.println("COUNT: " +count);
        return list;
 }//shuffle
  
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards];//declares a array
    for (int i= 0; i < numCards; i++){
      hand[i] = list[index];
      index--;}
    System.out.println("Hand");
    return hand;
  }//getHand
}//class