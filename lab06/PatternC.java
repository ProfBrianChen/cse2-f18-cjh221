//////////////
//// CSE 02 PatternC
/// Collin Hanlon
import java.util.Scanner; //scanner  

 public class PatternC {
      public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter a number between 1 and 10 "); //integer input of length
    int numInput = myScanner.nextInt();
    while ( numInput > 10 || numInput < 1) { //while to ensure valid input
        System.out.println("Error. Please reenter again");
        numInput = myScanner.nextInt();}
    for (int numRows = 1; numRows <= numInput; numRows++) { //format declaring patter
      int count = 1;
      int spaces;
      spaces = numInput - numRows;
      while (spaces > 0){
        System.out.print(" ");
        spaces = spaces -1;}
      int count2;
      count2 = numRows;
      while (count <= numRows){
        System.out.print(count2);
        count2 = count2-1;
        count = count+1;}
      count = 1;
      System.out.println(" ");}
}//end of main method 
}//end of class