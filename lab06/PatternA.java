//////////////
//// CSE 02 PatternA
/// Collin Hanlon
import java.util.Scanner; // import scanner
  public class PatternA {
      public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter a number between 1 and 10 "); //integer input of length
        int numInput = myScanner.nextInt();
      while ( numInput > 10 || numInput < 1) { //while to ensure valid input
        System.out.println("Error. Please reenter again");
        numInput = myScanner.nextInt();}
        for (int numRows = 1; numRows <= numInput; numRows++) { //format declaring pattern
          for (int colNum = 1; colNum <= numRows; colNum++) {
            System.out.print(colNum + " ");}
          System.out.println("");}
      }//end of main method 
  }//end of class