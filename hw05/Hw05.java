//////////////
//// CSE 002 HW05 Poker
/// Collin Hanlon
import java.util.Scanner; // Scanner
import java.util.Random; // random class
public class Hw05{
    public static void main(String args[]){
        int numHands; //initialize number of hands and final probability variables
        double probFour; double probThree; double probTwoPair; double probOnePair;
        int thisIsFour = 0; //int for every hand
        int thisIsThree = 0;
        int thisIsTwo = 0;
        int thisIsOne = 0;
        int countFour = 0; //int counters for every hand
        int countThree = 0;
        int countTwo = 0;
        int countOne = 0;
        Random rand = new Random(); //random number generator
        Scanner scan = new Scanner(System.in); //scanner
        int rand1; int rand2; int rand3; int rand4; int rand5; //generate random card numbers
        System.out.print("Number of hands drawn: "); //prompt for number of times
        numHands = scan.nextInt(); //how many hands have to be drawn
        System.out.println("");
        for (int i=0; i < numHands; ++i) { //loop for the amount of times inputted
            rand1 = rand.nextInt(52)+1; //creating random integers for the hand
            rand2 = rand.nextInt(52)+1;
            rand3 = rand.nextInt(52)+1;
            rand4 = rand.nextInt(52)+1;
            rand5 = rand.nextInt(52)+1;      
            thisIsOne = 0; //reset to 0
            thisIsTwo = 0;
            thisIsThree = 0;
            thisIsFour = 0;

            // When the first 2 match
            if (rand1 % 13 == rand2 % 13) {
                //where first three match
                if (rand2 % 13 == rand3 % 13) { //if there is a three of a kind
                    if (rand3 % 13 == rand4 % 13) { //if there is four of a kind
                        thisIsFour += 1;} //adding one to counter (four of a kind)
                    else if (rand3 % 13 == rand5 % 13) { //if matching except number 4
                        thisIsFour +=1;}
                    else if (rand3 %13 != rand4 %13 && rand3 %13 != rand5 %13 ) { //not counting situations of full house
                        thisIsThree += 1;}
                    else if ((rand3 %13 != rand4 %13 && rand3 %13 != rand5 %13)  && rand4 %13 == rand5 %13) { //full house
                        thisIsOne +=1;
                        thisIsThree +=1;}}
                //first two match, not with third
                else if (rand3 % 13 == rand4 % 13) { //if 4th and 5th match
                    if (rand1 % 13 != rand5% 13 && rand4 %13 != rand5 %13) { //not counting full house
                        thisIsTwo += 1;}
                    else if (rand1 % 13 == rand5% 13 && rand4 %13 == rand5 %13) { //if full house
                        thisIsOne +=1;
                        thisIsThree +=1;}}
                else if (rand4%13 == rand5%13) {
                    thisIsTwo +=1;}}

            //When second and third match
            else if (rand2 % 13 == rand3 % 13) { //2nd and 3rd matching
                //2nd-4th match
                if (rand3 % 13 == rand4 % 13) { 
                    if (rand4 % 13 == rand5 % 13) { //last 4 cards are matching
                        thisIsFour += 1;}
                    else if (rand1 % 13 != rand5 %13){ //not full house
                        thisIsThree += 1;}
                    else if (rand1 % 13 == rand5 %13) { //full house
                        thisIsThree +=1;
                        thisIsOne +=1;}}
                //if 2nd and 3rd match, but not 3rd and 4th
                else if (rand4 % 13 == rand5 % 13 && rand1 %13 != rand4 %13) { //matching but not a full house
                    thisIsTwo += 1;}
                else if (rand1 % 13 == rand4 % 13 && rand1 %13 != rand5 %13) {  //not a full house
                    thisIsTwo += 1;}
                else if (rand1 % 13 == rand4 % 13 && rand1 %13 == rand5 %13) { //if a full hosue
                    thisIsThree +=1;
                    thisIsOne +=1;}
                else {
                    thisIsOne += 1;}}

            //Where 3rd and 4th match
            else if (rand3 % 13 == rand4 % 13) {
                if (rand4 % 13 == rand5 % 13) {
                    thisIsThree += 1;}
                else if (rand1 % 13 == rand5 % 13) { //determining if 2 pair
                    thisIsTwo +=1;}
                else if (rand2 % 13 == rand5 % 13) { //checking if there is another pair
                    thisIsTwo +=1;}
                else {
                    thisIsOne += 1;}}

            //Where 4th and 5th mathc
            else if (rand4%13 ==rand5%13) {
                thisIsOne +=1;}

            //1st and 3rd matching
            else if (rand1 % 13 == rand3 % 13) {
                if (rand3 %13 ==rand4 %13 && rand3 %13 ==rand5 %13) { //first; 3-5 match
                    thisIsFour +=1;}
                else if (rand3 %13 ==rand4 %13 && rand3 %13 !=rand5 %13) { //first; 3-4 match
                    thisIsThree +=1;}
                else if (rand3 %13 ==rand4 %13 && rand3 %13 !=rand5 %13 && rand2 %13 == rand5%13) {//a full house
                    thisIsThree +=1;
                    thisIsOne +=1;}
                else if (rand1%13 != rand4%13 && rand2%13 != rand4%13 && rand4%13 == rand5%13) {//first and third match, 4th and 5th match
                    thisIsTwo +=1;}
                else if (rand2%13 ==rand4%13 && rand2%13 != rand5%13) { //first and third match, second and fourth match
                    thisIsTwo +=1;}
                else if (rand2%13 ==rand4%13 && rand2%13 == rand5%13) { //first and third match, second fourth and fifth match
                    thisIsThree +=1;
                    thisIsOne +=1;}}

            //Where 2nd and 4th
            else if (rand2%13 == rand4%13) {
                if (rand2 %13 ==rand5%13) {
                    thisIsThree +=1;}
                else if (rand1 % 13 != rand5%13 && rand3%13 != rand5%13) {
                    thisIsOne +=1;}
                else if (rand1%13 ==rand5%13) {
                    thisIsTwo +=1;}
                else if (rand3%13==rand5%13) {
                    thisIsTwo +=1;}}

            //3rd and 5th
            else if (rand3 % 13 == rand5 %13) {
                thisIsOne +=1;}

            //1st and 4th
            else if (rand1 % 13 == rand4%13) {
                if (rand4%13 != rand5 % 13) {
                    thisIsOne +=1;}
                else if (rand4%13 != rand5 % 13) {
                    thisIsThree +=1;}
                else if (rand2 %13 ==rand5%13) {
                    thisIsTwo +=1;}
                else if (rand3%13 ==rand5%13) {
                    thisIsTwo+=1;}}

            //2nd and 5th
            else if (rand2%13 == rand5%13) {
                thisIsOne +=1;}

            //1st and 5th match
            else if (rand1 %13 == rand5%13) {
                thisIsOne +=1;}

            if (thisIsOne > 0) {
                countOne +=1;}
            if (thisIsTwo > 0) {
                countTwo +=1;}
            if (thisIsThree > 0){
                countThree +=1;}
            if (thisIsFour > 0){
                countFour +=1;}
        }

    } 
    System.out.println("Number of four of a kinds: " + countFour); //printing the occurance of each
    System.out.println("Number of three of a kinds: " +countThree);
    System.out.println("Number of two pairs: " + countTwo);
    System.out.println("Number of one pairs: " + countOne);

    probFour = (double) countFour/ (double) numHands; //calculate probabilities of each
    probThree = (double)countThree/ (double)numHands; 
    probTwoPair = (double) countTwo/ (double) numHands;
    probOnePair = (double) countOne/ (double) numHands;

    System.out.println("The number of hands: " + numHands); //outputting the number of hands
    System.out.println("Probability of four-of-a-kind: " + probFour); //outputting the probabilities
    System.out.println("Probability of three-of-a-kind: " + probThree); 
    System.out.println("Probability of two-pair: " + probTwoPair);
    System.out.println("Probability of one-pair: " + probOnePair);
    System.out.println();
}//end of main method
}//end of class
