//////////////
//// CSE 002 HW06 WordTools
/// Collin Hanlon
//////////////

import java.util.Scanner; // import the scanner

public class WordTools { //class 
  
  public static void main(String[]args) { //main method
    Scanner myScanner = new Scanner(System.in); //declare the scanner
    String text = sampleText(); 
    int counter = 0; //initialize the counter
    
    while (counter == 0){
    char character = printMenu();
    switch (character) {
      case 'q': //user wants to quit
        counter = 1;
        break;
      case 'c': //user wants the numebr of non whitespace characters
        int valC = getNumOfNonWSCharacters(text); //call the method
        System.out.println("The number of non-whitespace characters is: " + valC); //print the result
        break;
      case 'w': //user wants the number of words in the sample text
        int valW = getNumOfWords(text); // call the method
        System.out.println("The number of words is: " + valW); //print out the numebr of words
        break;
      case 'f': //user would like to find a specific phrase
        System.out.println("Which word or phrase would you like to find? "); //user inputs which phrase they would like to find
        String find = myScanner.nextLine(); //scanner stores it
        int valF = findText(find, text); //calls the method
        System.out.println("\"" + find + "\"" + "instances: " + valF); //prints out the word and how many instances
        break;
      case 'r': //runs the exclamation replacement method
        String period = replaceExclamation(text); //call the methos
        System.out.println("Edited: "+ period); //print the edit
        break;
      case 's':
        String spaces = shortenSpaces(text); //runs the shorten spaces method
        System.out.println("Edited: " + spaces); //prints the edit
        break;
      default:
        System.out.println("Your input is invalid. Please select from the menu."); //catches an illegal input
        break; // return to the print menu
    
    } 
  }
 
  } 
  
  public static String sampleText() { //method for the sample text input
    Scanner myScanner = new Scanner(System.in); //declare the scanner
    
    System.out.println("Enter your sample text: "); //prompt user to input text
    String textSample = myScanner.nextLine(); // store the input as a string
    
    System.out.println(" ");
    System.out.println("Your input is: " + textSample);
    return textSample;
  }
  
  public static char printMenu() { //menu for different things you can do with the sample text
    System.out.println("c - Number of non-whitespace characters"); //counts whitespace
    System.out.println("w - Number of words"); //counts the numebr of words
    System.out.println("f - Find text"); 
    System.out.println("r - Replace all !'s"); //replace any exclamation marks in the text
    System.out.println("s - Shorten spaces"); 
    System.out.println("q - quit"); //will continue to ask for functions until the user quits the program with the q
                         
    Scanner myScanner = new Scanner(System.in); //declare the scanner in new method
    System.out.println("Choose an option: "); //prompt user to input a letter from above options
    
    String choice = myScanner.nextLine(); //user input is a string
    char choiceChar = 'x'; //store this variable temporarily for initialization
    
    if (choice.length() != 1) { //user cannot input for than one letter
      choiceChar = 'x'; //char choice x is not valid, therefore the user will be prmpted again
    }
    else { // one letter is inputted
      choiceChar = choice.charAt(0); // character and letter are set equal
    }
    return choiceChar;
  }
  
  public static int getNumOfNonWSCharacters(String text) {
    int limUpper = text.length()-1; //initialize the upper limit of the loop
    int counter = 0; // initialize counter
    
    for (int i = 0; i <= limUpper; i++) { //counter continues until it reaches the upper limit
      char test = text.charAt(i);
      if(!Character.isWhitespace(test)){
         counter = counter + 1;
      }
    }
    return counter;
  }
  
  
  public static int getNumOfWords(String text) {
    int limUpper = text.length()-1; // initialize the upper limit
    int counter = 0; // initialize counter
    
    
    for(int i = 0; i <= limUpper; i++) {
      char test = text.charAt(i);
      if(Character.isWhitespace(test)) {
       char test2 = text.charAt(i-1); 
       if(!Character.isWhitespace(test2)){
         counter = counter + 1;
       }
      }
    }
    counter = counter + 1;
    return counter;
  }
  
  
  public static int findText(String find, String text) {
    int counter = 0; //initialize the counter
    
    if (text.contains(find)) { //check that the input is in the sample text
      int length = text.length(); //get the length of the text
      String text1 = text.replace(find, ""); // replace all the inputs with nothing
      int length1 = text1.length(); // get the length after removing all of the inputs
      counter = (length-length1)/(find.length()); //equaiton to give the numebr of times the find input was in the text
    }
    return counter; //return the counter
  }
  
  
  public static String replaceExclamation(String text) {
    String textEdit = text.replace("!", "."); //replace the double space with a single space
    return textEdit; //returns the text without exclamations
  }
  
  
  public static String shortenSpaces(String text) {
    String textEdit2 = text.replace("  ", " "); //replace the double space with a single space
    return textEdit2; //returns the single space text
  }
  
  
 }