//////////////
//// CSE 02 UserInput
/// Collin Hanlon
import java.util.Scanner; // scanner import 
public class UserInput{
      public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
          System.out.println("Input the course number of said course: "); // course inputs
          boolean course = myScanner.hasNextInt(); // use a boolean to make sure an int is entered
        while (course == false){ // while state to change the input until correct
          System.out.println("Incorrect input. Please retry.");
          String junk = myScanner.next(); // throw out the incorrect input
          System.out.println("Input your course number: ");
          course = myScanner.hasNextInt();}
        if(course){
          int courseNumber = myScanner.nextInt();} // course number
        
          System.out.println("Input the department name: ");
          boolean name = myScanner.hasNextInt(); // use a boolean for verification
        while (name == true){ // correct with while
          System.out.println("Incorrect input. Please retry.");
          String junk = myScanner.next(); // throw out the incorrect input
          System.out.println("Input the department name: ");
          name = myScanner.hasNextInt();}
        if(name == false){
          String departmentName = myScanner.next();} // department name

        System.out.println("Input number of times it meets in a week: ");
          boolean meets = myScanner.hasNextInt(); // use a boolean for verification of type
        while (meets == false){
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out incorrect inputs
          System.out.println("Input number of times it meets in a week: ");
          meets = myScanner.hasNextInt();}
        if(meets){
          int meetTimes = myScanner.nextInt();} // meeting times
        
        System.out.println("Input the time class starts (10:30 = 10.30): ");
          boolean time = myScanner.hasNextDouble(); // use a boolean for verification
        while (time == false){
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out incorrect inputs
          System.out.println("Input the time class starts: ");
          time = myScanner.hasNextDouble();}
        if(time){
          double startTime = myScanner.nextDouble();} // start time
        
        System.out.println("Input the instructor's name: ");
          boolean prof = myScanner.hasNextInt(); // use a boolean for verification of input
        while (prof == true){ 
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out incorrect inputs
          System.out.println("Input the instructor's name: ");
          prof = myScanner.hasNextInt();}
        if(prof == false){
          String professorName = myScanner.next();} // professor name
      }//end of main method 
  }//end of class