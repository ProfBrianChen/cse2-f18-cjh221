//////////////
//// CSE 002 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    /// prints Welcome and Lehigh ID as a signature along with an autobiographic statement
    System.out.println("    -----------  ");
    System.out.println("    | WELCOME |  ");
    System.out.println("    -----------  ");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-C--J--H--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I am a sophomore from Yardley, Pennsylvania (North of Philadelphia) pursuing a degree in Industrial and Systems engineering, I am also a member of the mens varsity swim team and enjoy writing python scripts in my free time utilizing cloud storage through linux and apache.");
                       
  }
                       
}