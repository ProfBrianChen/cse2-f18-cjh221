//////////////
//// CSE 002 CrapsIf
/// Collin Hanlon
import java.util.Scanner;
import java.util.Random;
public class CrapsIf{ //generating or inputting a pair of values for craps and finding the slang for it
  public static void main(String args[]){
    Scanner myScan = new Scanner (System.in); //create a scanner prompting user for choice of craps
    Random rolls = new Random(); //create a random generator for non-inputted dice
    System.out.print("Do you want randomly generated numbers for the die (press 1), or input rolled values (press 2) ?"); //ask to generate numbers or not
    int choice = myScan.nextInt(); //collect input
    int die1, die2; //inputted rolled numbers
    
    if (choice==1){ //for generated numbers
      die1 = rolls.nextInt(6)+1; //generating roll for die 1
      die2 = rolls.nextInt(6)+1; //generating roll for die 2
      System.out.println("Die 1: " + die1); 
      System.out.println("Die 2: " + die2);
      if (die1==1){ //slang terminology if die 1 is a 1
        if(die2==1){
          System.out.println("Snake eyes");}
        else if (die2==2){
          System.out.println("Ace Deuce");}
        else if (die2==3){
          System.out.println("Easy Four");}
        else if (die2==4){
          System.out.println("Fever Five");}
        else if (die2==5){
          System.out.println("Easy Six");}
        else if (die2==6){
          System.out.println("Seven Out");}}
      if (die1==2){ //slang terminology if die 1 is a 2
        if (die2==1){
          System.out.println("Ace Deuce");}
        else if (die2==2){
          System.out.println("Hard Four");}
        else if (die2==3){
          System.out.println("Fever Five");}
        else if (die2==4){
          System.out.println("Easy Six");}
        else if (die2==5){
          System.out.println("Seven Out");}
        else if (die2==6){
          System.out.println("Easy Eight");}}
      if (die1==3){ //slang terminology if die 1 is a 3
        if (die2==1){
          System.out.println("Easy Four");}
        else if (die2==2){
          System.out.println("Fever Five");}
        else if (die2==3){
          System.out.println("Hard Six");}
        else if (die2==4){
          System.out.println("Seven Out");}
        else if (die2==5){
          System.out.println("Easy Eight");}
        else if (die2==6){
          System.out.println("Nine");}}
      if (die1==4){ //slang terminology if die 1 is a 4
        if (die2==1){
          System.out.println("Fever Five");}
        else if (die2==2){
          System.out.println("Easy Six");}
        else if (die2==3){
          System.out.println("Seven Out");}
        else if (die2==4){
          System.out.println("Hard Eight");}
        else if (die2==5){
          System.out.println("Nine");}
        else if (die2==6){
          System.out.println("Easy Ten");}}
      if (die1==5){ //slang terminology if die 1 is a 5
        if (die2==1){
          System.out.println("Easy Six");}
        else if (die2==2){
          System.out.println("Seven Out");}
        else if (die2==3){
          System.out.println("Easy Eight");}
        else if (die2==4){
          System.out.println("Nine");}
        else if (die2==5){
          System.out.println("Hard Ten");}
        else if (die2==6){
          System.out.println("Yo-leven");}}
      if (die1==6){ //slang terminology if die 1 is a 6
        if (die2==1){
          System.out.println("Seven Out");}
        else if (die2==2){
          System.out.println("Easy Eight");}
        else if (die2==3){
          System.out.println("Seven Out");}
        else if (die2==4){
          System.out.println("Easy Ten");}
        else if (die2==5){
          System.out.println("Yo-leven");}
        else if (die2==6){
          System.out.println("Boxcars");}}
    }
    else if (choice==2){
      System.out.print("What is the first die number?: "); //the rolled first die
      die1 = myScan.nextInt(); //collecting die 1
      System.out.print("What is the second die number?: "); //the rolled second die
      die2 = myScan.nextInt(); //collecting die 2
      System.out.println("Die 1: " + die1);
      System.out.println("Die 2: " + die2);
      if (die1==1){ //slang terminology if die 1 is a 1
        if(die2==1){
          System.out.println("Snake eyes");}
        else if (die2==2){
          System.out.println("Ace Deuce");}
        else if (die2==3){
          System.out.println("Easy Four");}
        else if (die2==4){
          System.out.println("Fever Five");}
        else if (die2==5){
          System.out.println("Easy Six");}
        else if (die2==6){
          System.out.println("Seven Out");}}
      if (die1==2){ //slang terminology if die 1 is a 2
        if (die2==1){
          System.out.println("Ace Deuce");}
        else if (die2==2){
          System.out.println("Hard Four");}
        else if (die2==3){
          System.out.println("Fever Five");}
        else if (die2==4){
          System.out.println("Easy Six");}
        else if (die2==5){
          System.out.println("Seven Out");}
        else if (die2==6){
          System.out.println("Easy Eight");}}
      if (die1==3){ //slang terminology if die 1 is a 3
        if (die2==1){
          System.out.println("Easy Four");}
        else if (die2==2){
          System.out.println("Fever Five");}
        else if (die2==3){
          System.out.println("Hard Six");}
        else if (die2==4){
          System.out.println("Seven Out");}
        else if (die2==5){
          System.out.println("Easy Eight");}
        else if (die2==6){
          System.out.println("Nine");}}
      if (die1==4){ //slang terminology if die 1 is a 4
        if (die2==1){
          System.out.println("Fever Five");}
        else if (die2==2){
          System.out.println("Easy Six");}
        else if (die2==3){
          System.out.println("Seven Out");}
        else if (die2==4){
          System.out.println("Hard Eight");}
        else if (die2==5){
          System.out.println("Nine");}
        else if (die2==6){
          System.out.println("Easy Ten");}}
      if (die1==5){ //slang terminology if die 1 is a 5
        if (die2==1){
          System.out.println("Easy Six");}
        else if (die2==2){
          System.out.println("Seven Out");}
        else if (die2==3){
          System.out.println("Easy Eight");}
        else if (die2==4){
          System.out.println("Nine");}
        else if (die2==5){
          System.out.println("Hard Ten");}
        else if (die2==6){
          System.out.println("Yo-leven");}}
      if (die1==6){ //slang terminology if die 1 is a 6
        if (die2==1){
          System.out.println("Seven Out");}
        else if (die2==2){
          System.out.println("Easy Eight");}
        else if (die2==3){
          System.out.println("Seven Out");}
        else if (die2==4){
          System.out.println("Easy Ten");}
        else if (die2==5){
          System.out.println("Yo-leven");}
        else if (die2==6){
          System.out.println("Boxcars");}}   
    }
    else { //number picked isnt an option
      System.out.println("That was not a valid option");}
  } //end of main method
} //end of class