//////////////
//// CSE 02 CrapsSwitch
/// Collin Hanlon
import java.util.Scanner;
import java.util.Random;
public class CrapsSwitch { //generating or inputting a pair of values for craps and finding the slang for it
  public static void main(String args[]){
    Scanner myScan = new Scanner (System.in); //create a scanner prompting user for choice of craps
    Random rolls = new Random(); //create a random generator for non-inputted dice
    System.out.print("Do you want randomly generated numbers for the die (press 1), or input rolled values (press 2) ?"); //ask to generate numbers or not
    int choice = myScan.nextInt(); //collect input
    int die1, die2; //inputted rolled numbers
   
   if (choice==1){ //for generated numbers
      die1 = rolls.nextInt(6)+1; //generating roll for die 1
      die2 = rolls.nextInt(6)+1; //generating roll for die 2
      System.out.println("Die 1: " + die1); 
      System.out.println("Die 2: " + die2);
      if (die1==1){ //slang terms for die 1 is a 1
        switch (die2) {
          case 1 :
          System.out.println("Snake eyes");
          break;
          case 2: 
          System.out.println("Ace Deuce");
          break;
          case 3: 
          System.out.println("Easy Four");
          break;
          case 4: 
          System.out.println("Fever Five");
          break;
          case 5: 
          System.out.println("Easy Six");
          break;
          case 6: 
          System.out.println("Seven Out");
          break;}}
      if (die1==2){ //slang terms for die 1 is a 2
        switch (die2){
          case 1:
          System.out.println("Ace Deuce");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3: 
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6: 
          System.out.println("Easy Eight");
          break;}}
      if (die1==3){ //slang terms for die 1 is a 3
        switch (die2){
          case 1:
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4: 
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:
          System.out.println("Nine");
          break;}}
      if (die1==4){ //slang terms for die 1 is a 4
        switch(die2){
          case 1:
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          break;
          case 6:
          System.out.println("Easy Ten");
          break;}}
      if (die1==5){ //slang terms for die 1 is a 5
        switch (die2){
          case 1:
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:
          System.out.println("Yo-leven");
          break;}}
      if (die1==6){ //slang terms for die 1 is a 6
        switch(die2){
          case 1:
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Nine");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:
          System.out.println("Boxcars");
          break;}}
    }
    else if (choice==2){ //if inputted numbers
      System.out.print("What is the first die number?: "); //the first die rolled
      die1 = myScan.nextInt(); //collecting die 1
      System.out.print("What is the second die number?: "); //the second die rolled
      die2 = myScan.nextInt(); //collecting die 2
      System.out.println("Die 1: " + die1);
      System.out.println("Die 2: " + die2);
       if (die1==1){//slang terms for die 1 is a 1
        switch (die2) {
          case 1 :
          System.out.println("Snake eyes");
          break;
          case 2: 
          System.out.println("Ace Deuce");
          break;
          case 3: 
          System.out.println("Easy Four");
          break;
          case 4: 
          System.out.println("Fever Five");
          break;
          case 5: 
          System.out.println("Easy Six");
          break;
          case 6: 
          System.out.println("Seven Out");
          break;}}
      if (die1==2){//slang terms for die 1 is a 2
        switch (die2){
          case 1:
          System.out.println("Ace Deuce");
          break;
          case 2:
          System.out.println("Hard Four");
          break;
          case 3: 
          System.out.println("Fever Five");
          break;
          case 4:
          System.out.println("Easy Six");
          break;
          case 5:
          System.out.println("Seven Out");
          break;
          case 6: 
          System.out.println("Easy Eight");
          break;}}
      if (die1==3){//slang terms for die 1 is a 3
        switch (die2){
          case 1:
          System.out.println("Easy Four");
          break;
          case 2:
          System.out.println("Fever Five");
          break;
          case 3:
          System.out.println("Hard Six");
          break;
          case 4: 
          System.out.println("Seven Out");
          break;
          case 5:
          System.out.println("Easy Eight");
          break;
          case 6:
          System.out.println("Nine");
          break;}}
      if (die1==4){//slang terms for die 1 is a 4
        switch(die2){
          case 1:
          System.out.println("Fever Five");
          break;
          case 2:
          System.out.println("Easy Six");
          break;
          case 3:
          System.out.println("Seven Out");
          break;
          case 4:
          System.out.println("Hard Eight");
          break;
          case 5:
          System.out.println("Nine");
          break;
          case 6:
          System.out.println("Easy Ten");
          break;}}
      if (die1==5){//slang terms for die 1 is a 5
        switch (die2){
          case 1:
          System.out.println("Easy Six");
          break;
          case 2:
          System.out.println("Seven Out");
          break;
          case 3:
          System.out.println("Easy Eight");
          break;
          case 4:
          System.out.println("Nine");
          break;
          case 5:
          System.out.println("Hard Ten");
          break;
          case 6:
          System.out.println("Yo-leven");
          break;}}
      if (die1==6){//slang terms for die 1 is a 6
        switch(die2){
          case 1:
          System.out.println("Seven Out");
          break;
          case 2:
          System.out.println("Easy Eight");
          break;
          case 3:
          System.out.println("Nine");
          break;
          case 4:
          System.out.println("Easy Ten");
          break;
          case 5:
          System.out.println("Yo-leven");
          break;
          case 6:
          System.out.println("Boxcars");
          break;}}
    }
    else { //number inputted is not an option
      System.out.println("That was not a valid option");}
  } //end of main method
} //end of class
