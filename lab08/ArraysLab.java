//////////////
//// CSE 02 Arrays Lab
/// Collin Hanlon
//////////////
import java.util.Random; //Random
import java.util.Arrays; //arrays
public class ArraysLab{ //class
  public static void main(String[] args){ //main
    int [] arrayOne= new int[100]; //array1
    int [] arrayTwo= new int[100]; //array2
    
    Random rand = new Random(); //randomly generating 100 int
    for (int i =0; i<arrayOne.length; i++) {
      int n = rand.nextInt(100);
      arrayOne[i] = n;}
    
    System.out.println("array 1 holds the following integers: " + Arrays.toString(arrayOne)); //output contents of array
    int j=0; //values
    int i=0;
    
    for (i=0; i < arrayOne.length; i++){
      j=arrayOne[i];
      arrayTwo[j]++;
    }
    for (i=1; i <arrayTwo.length; i++){ //the # of times a num is found
      if(arrayTwo[i] == 1){
        System.out.println(i + " occurs " + arrayTwo [i] + " time " );
      }
      else if(arrayTwo[i] >=2 || arrayTwo[i] == 0){
        System.out.println(i + " occurs " + arrayTwo [i] + " times ");
      }
    }
  }
}